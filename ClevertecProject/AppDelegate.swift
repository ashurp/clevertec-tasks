//
//  AppDelegate.swift
//  ClevertecProject
//
//  Created by Alexandra Shurpeleva on 31.01.22.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let currentLanguage = UserDefaults.standard.string(forKey: "i18n_language")
        Bundle.setLanguage(lang: "en")
        switch currentLanguage {
        case "be-BY":
            Bundle.setLanguage(lang: "be-BY")
        case "ru":
            Bundle.setLanguage(lang: "ru")
        default:
            Bundle.setLanguage(lang: "en")
        }

        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

}

