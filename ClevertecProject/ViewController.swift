//
//  ViewController.swift
//  ClevertecProject
//
//  Created by Alexandra Shurpeleva on 31.01.22.
//

import UIKit

class ViewController: UIViewController {
    let pickerView = UIPickerView()
    let greetingLabel = UILabel()
    let lightButton = UIButton(title: Theme.light.rawValue.capitalized.localized,
                               fontSize: 20, cornerRadius: 4)
    let darkButton = UIButton(title: Theme.dark.rawValue.capitalized.localized,
                              fontSize: 20, cornerRadius: 4)
    let autoButton = UIButton(title: Theme.auto.rawValue.capitalized.localized,
                              fontSize: 20, cornerRadius: 4)

    override func viewDidLoad() {
        super.viewDidLoad()
        setupLayout()
    }
}

extension ViewController {

    // MARK: - Screen Layout

    func setupLayout() {
        view.backgroundColor = .systemBackground

        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.backgroundColor = .clear

        let rocketLogo = UIImage(named: ImageName.rocket.rawValue)
        let logo = UIImageView(image: rocketLogo)

        greetingLabel.text = LabelName.greeting.rawValue.capitalized.localized
        greetingLabel.font = UIFont(name: "AvenirNext-Regular", size: 20)
        greetingLabel.textAlignment = .center
        greetingLabel.numberOfLines = 0

        lightButton.addTarget(self, action: #selector(lightButtonTapped), for: .touchUpInside)
        darkButton.addTarget(self, action: #selector(darkButtonTapped), for: .touchUpInside)
        autoButton.addTarget(self, action: #selector(autoButtonTapped), for: .touchUpInside)

        let buttonStack = UIStackView(distribution: .fillEqually, spacing: 12,
                                      elements: [lightButton, darkButton, autoButton])
        let mainStackView = UIStackView(axis: .vertical, spacing: 20,
                                        elements: [logo, greetingLabel, pickerView, buttonStack])
        
        logo.setViewLayout(height: 64, width: 64)
        greetingLabel.setViewLayout(leading: 0, trailing: 0)
        pickerView.setViewLayout(leading: 0, trailing: 0)
        buttonStack.setViewLayout(leading: 0, trailing: 0)

        view.addSubview(mainStackView)
        let screenHeight = UIScreen.main.bounds.height
        mainStackView.setViewLayout(leading: 20, trailing: 20, top: screenHeight / 8)
    }

    // MARK: - Button Actions

    @objc func lightButtonTapped() {
        lightButton.isSelected = true
        darkButton.isSelected = false
        autoButton.isSelected = false

        view.window?.overrideUserInterfaceStyle = .light
    }

    @objc func darkButtonTapped() {
        lightButton.isSelected = false
        darkButton.isSelected = true
        autoButton.isSelected = false

        view.window?.overrideUserInterfaceStyle = .dark
    }

    @objc func autoButtonTapped() {
        lightButton.isSelected = false
        darkButton.isSelected = false
        autoButton.isSelected = true

        view.window?.overrideUserInterfaceStyle = .unspecified
    }

    // MARK: - Update Labels

    func updateLocalization() {
        greetingLabel.text = LabelName.greeting.rawValue.capitalized.localized
        lightButton.setTitle(Theme.light.rawValue.capitalized.localized, for: .normal)
        darkButton.setTitle(Theme.dark.rawValue.capitalized.localized, for: .normal)
        autoButton.setTitle(Theme.auto.rawValue.capitalized.localized, for: .normal)
    }
}

// MARK: - UIPickerView settings

extension ViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return Language.allCases.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let values: [String] =  Language.allCases.map { $0.rawValue.capitalized }
        return values[row]
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let values: [String] =  Language.allCases.map { $0.rawValue.capitalized }
        var newLanguage = "en"

        if values[row] == Language.russian.rawValue {
            newLanguage = "ru"
        } else if values[row] == Language.belarusian.rawValue {
            newLanguage = "be-BY"
        }

        UserDefaults.standard.set(newLanguage, forKey: "i18n_language")
        Bundle.setLanguage(lang: newLanguage)
        self.updateLocalization()
    }
}
