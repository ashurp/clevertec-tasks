//
//  UIButton+Extension.swift
//  ClevertecProject
//
//  Created by Alexandra Shurpeleva on 31.01.22.
//

import Foundation
import UIKit

extension UIButton {
    convenience init(backgroundColor: UIColor? = .clear, title: String, fontSize: CGFloat = 18, cornerRadius: CGFloat, contentMode: UIView.ContentMode = .scaleAspectFill) {
        self.init(type: .system)
        self.backgroundColor = backgroundColor
        self.setTitle(title, for: .normal)
        self.setTitleColor(UIColor.label, for: .normal)
        self.titleLabel?.font = UIFont(name: "AvenirNext-Regular", size: fontSize)
        self.cornerRadius = cornerRadius
        self.contentMode = contentMode
    }
}
