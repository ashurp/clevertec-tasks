//
//  UIStackView+Extension.swift
//  ClevertecProject
//
//  Created by Alexandra Shurpeleva on 1.02.22.
//

import Foundation
import UIKit

extension UIStackView {
    convenience init(frame: CGRect = .zero,
                     axis: NSLayoutConstraint.Axis = .horizontal,
                     distribution: Distribution = .fill,
                     alignment: Alignment = .center,
                     spacing: CGFloat,
                     backgroundColor: UIColor? = .clear,
                     elements: [UIView] = []) {
        self.init(arrangedSubviews: elements)
        self.frame = frame
        self.axis = axis
        self.distribution = distribution
        self.alignment = alignment
        self.spacing = spacing
        self.backgroundColor = backgroundColor
    }
}
