//
//  UIView+Extension.swift
//  ClevertecProject
//
//  Created by Alexandra Shurpeleva on 31.01.22.
//

import Foundation
import UIKit

extension UIView {

    // MARK: - View Radius

    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }

    // MARK: - Top Radius

    var topCornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
            layer.cornerRadius = newValue
        }
    }

    // MARK: - Bottom Radius

    var bottomCornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
            layer.cornerRadius = newValue
        }
    }

    // MARK: - Border Width

    var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }

    // MARK: - Border Color

    var borderColor: UIColor? {
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.borderColor = color.cgColor
            } else {
                layer.borderColor = nil
            }
        }
    }
}
