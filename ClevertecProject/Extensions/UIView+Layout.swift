//
//  UIView+Layout.swift
//  ClevertecProject
//
//  Created by Alexandra Shurpeleva on 31.01.22.
//

import Foundation
import UIKit

extension UIView {
    func setViewLayout(leading: CGFloat? = nil, trailing: CGFloat? = nil, top: CGFloat? = nil, bottom: CGFloat? = nil, height: CGFloat? = nil, width: CGFloat? = nil) {
        guard let superview = self.superview else {
            assertionFailure("The view was not added to hierarchy before setting up layout")
            return
        }

        self.translatesAutoresizingMaskIntoConstraints = false

        if let leading = leading {
            self.leadingAnchor.constraint(equalTo: superview.leadingAnchor, constant: leading)
                .isActive = true
        }

        if let trailing = trailing {
            self.trailingAnchor.constraint(equalTo: superview.trailingAnchor, constant: -trailing)
                .isActive = true
        }

        if let top = top {
            self.topAnchor.constraint(equalTo: superview.topAnchor, constant: top)
                .isActive = true
        }

        if let bottom = bottom {
            self.bottomAnchor.constraint(equalTo: superview.bottomAnchor, constant: -bottom)
                .isActive = true
        }

        if let height = height {
            self.heightAnchor.constraint(equalToConstant: height).isActive = true
        }

        if let width = width {
            self.widthAnchor.constraint(equalToConstant: width).isActive = true
        }
    }
}
