//
//  Enumerators.swift
//  ClevertecProject
//
//  Created by Alexandra Shurpeleva on 1.02.22.
//

import Foundation

enum ImageName: String {
    case rocket
}

enum LabelName: String, CaseIterable {
    case greeting
}

enum Language: String, CaseIterable {
    case english
    case belarusian = "Беларуская"
    case russian = "Русский"
}

enum Theme: String {
    case light
    case dark
    case auto
}
