//
//  String+Extension.swift
//  ClevertecProject
//
//  Created by Alexandra Shurpeleva on 4.02.22.
//

import Foundation

extension String {
    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.localizedBundle(), value: "", comment: "")
    }
}
